smf_api_2.php integration module
==============================================================================

smf_api_2.php provides advanced integration with SMF: Simple Machines Forum
http://www.simplemachines.org/.

Download the smf_api_2 archive from http://vgb.org.ru/files/smf_api_2.zip
Unpack smf_api_2 archive.
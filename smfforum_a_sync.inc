<?php
/**
 * Copyright 2008 by Vadim G.B. (http://vgb.org.ru)
 */

function _smfforum_a_sync_set($val) {
  if (module_exists('a_sync'))
    module_invoke('a_sync', 'set', 'smfforum', 'user', $val);
}

function _smfforum_a_sync_get() {
  if (module_exists('a_sync'))
    return module_invoke('a_sync', 'get', 'user');
  return 0;  
}

function _smfforum_a_sync_get_module($var) {
  if (module_exists('a_sync'))
    return module_invoke('a_sync', 'get_module', $var);
  return false;
}

function _smfforum_a_sync_get_sync_to() {
  if (module_exists('a_sync'))
    return module_invoke('a_sync', 'get_sync_to', 'smfforum', 'user');
  return 0;  
}

function _smfforum_get_a_sync_to($syncto) {
  
  $module = _smfforum_a_sync_get_module('user');
  if ($module === false)
    return $syncto;
  
  if ($module != 'smfforum' && $module != '') {
    $a_syncto = _smfforum_a_sync_get_sync_to();
    if ($a_syncto == A_SYNC_TO_ME) {
      $syncto = SMF_SYNC_TO_DRUPAL;
    }  
    else {
      $syncto = SMF_SYNC_TO_SMF;
    }  
  }  
  
  return $syncto;  
}

function _smfforum_set_a_sync_to($sync) {
  
  $syncto = $sync;  
  if ($sync == SMF_SYNC_TO_MASTER) {
    $master = variable_get('smfforum_master', 1);
    if ($master) {
      $syncto = SMF_SYNC_TO_DRUPAL;
    }  
    else {
      $syncto = SMF_SYNC_TO_SMF;
    }  
  }
  
  $module = _smfforum_a_sync_get_module('user');
  if ($module === false)
    return $syncto;
  
  if ($module == '') {
    if ($syncto == SMF_SYNC_TO_DRUPAL) {
      $a_syncto = A_SYNC_TO_ME;
    }  
    else {
      $a_syncto = A_SYNC_TO_EXTERNAL;
    }
    _smfforum_a_sync_set($a_syncto);
  }
  
  return $syncto;  
}

